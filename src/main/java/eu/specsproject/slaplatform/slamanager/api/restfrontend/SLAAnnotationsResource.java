/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.api.restfrontend;


import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import eu.specsproject.slaplatform.slamanager.api.restbackend.SLAAPI;
import eu.specsproject.slaplatform.slamanager.entities.Annotation;
import eu.specsproject.slaplatform.slamanager.entities.SLAIdentifier;

public class SLAAnnotationsResource {
    
    private SLAIdentifier id ;

    public SLAAnnotationsResource (SLAIdentifier id){
        this.id=id;
    }

    @GET
    public Response getAnnotations(){
        
         GenericEntity<List<Annotation>> coll = new GenericEntity<List<Annotation>>(SLAAPI.getInstance().getManager().getAnnotations(id)) {
             //Anonimous class
         };

         return Response.ok(coll).build();        
    }
    
    
    @POST
    public void createAnnotation(@FormParam("id") String annotationId,
            @FormParam("description") String description, @FormParam("timestamp") String timestamp){
        
        Annotation ann = new Annotation(annotationId, description, timestamp);
        
        SLAAPI.getInstance().getManager().annotate(id, ann);
    }
    
    
}
