/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.api.restfrontend;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import eu.specsproject.slaplatform.slamanager.api.restbackend.SLAAPI;
import eu.specsproject.slaplatform.slamanager.entities.SLADocument;
import eu.specsproject.slaplatform.slamanager.entities.SLAIdentifier;


public class SLAResource {

    private SLAIdentifier id ;

    public SLAResource (String id){
        this.id=new SLAIdentifier(id);
    }
    
    @GET
    @Produces(MediaType.TEXT_XML)
    public String getSLA(){
        return SLAAPI.getInstance().getManager().retrieve(id).getSlaXmlDocument();
    }
    
    @PUT
    @Consumes(MediaType.TEXT_XML)
    public String updateSLA(String newSla){

        SLADocument xml= null;
        
        if (newSla!=null){
            xml = new SLADocument(newSla);
            SLAAPI.getInstance().getManager().update(id, xml, null);    
        }
        return id.id;
    }
    
    @DELETE
    public String deleteSLA(){
        SLAAPI.getInstance().getManager().remove(id);    
        return id.id;
    }
    
    @Path("/lock")
    @GET
    public String getAndLockSLA(){
        return SLAAPI.getInstance().getManager().retrieveAndLock(id).toString();
    }
    
    
    @Path("/annotations")
    public SLAAnnotationsResource getAnnotationResource(){
        return new SLAAnnotationsResource(id);
    }
    
    @Path("/status")
    @GET
    public String getSLAstatus(){

        return SLAAPI.getInstance().getManager().getState(id).toString();
    }
    
    @Path("/customer")
    @GET
    public String getSLAcustomer(){

        return SLAAPI.getInstance().getManager().retrieveCustomerFromSla(id).toString();
    }

    @Path("/sign")
    @POST
    @Consumes(MediaType.TEXT_XML)
    public void signSLA(String newSla){
        
        SLADocument xml= null;
        
        if (newSla!=null && (!"".equalsIgnoreCase(newSla))){
            xml = new SLADocument(newSla);
        }
        
        SLAAPI.getInstance().getManager().sign(id, xml, null);      
    }
    
    @Path("/userSign")
    @POST
    @Consumes(MediaType.TEXT_XML)
    public void userSignSLA(String newSla){
        
        SLADocument xml= null;
        
        if (newSla!=null && (!"".equalsIgnoreCase(newSla))){
            xml = new SLADocument(newSla);
        }
        
        SLAAPI.getInstance().getManager().userSign(id, xml, null);      
    }
    
    @Path("/implement")
    @POST
    @Consumes(MediaType.TEXT_XML)
    public void implementSLA(String newSla){
        SLAAPI.getInstance().getManager().implement(id);      
    }

    @Path("/observe")
    @POST
    public void observeSLA(){
        SLAAPI.getInstance().getManager().observe(id);      
    }
    
    @Path("/complete")
    @POST
    public void completeSLA(){
        SLAAPI.getInstance().getManager().complete(id);     
    }

    @Path("/terminate")
    @POST
    public void terminateSLA(){
        SLAAPI.getInstance().getManager().terminate(id);        
    }


    @Path("/signalAlert")
    @POST
    public void signalAlert(){
        SLAAPI.getInstance().getManager().signalAlert(id);        
    }
    
    @Path("/signalViolation")
    @POST
    public void signalViolation(){
        SLAAPI.getInstance().getManager().signalViolation(id);        
    }

    @Path("/reNegotiate")
    @POST
    public void reNegotiate(){
        SLAAPI.getInstance().getManager().reNegotiate(id);      
    }
    
    @Path("/remediate")
    @POST
    public void remediate(){
        SLAAPI.getInstance().getManager().remediate(id);      
    }
    
    @Path("/redress")
    @POST
    public void redress(){
        SLAAPI.getInstance().getManager().redress(id);      
    }
    
    @Path("/unlock")
    @POST 
    public void unlockSLA(){
        SLAAPI.getInstance().getManager().unlock(id,null);      
    }

}
