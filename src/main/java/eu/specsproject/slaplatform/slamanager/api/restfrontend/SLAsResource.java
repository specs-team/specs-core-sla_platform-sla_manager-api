/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.api.restfrontend;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import eu.specsproject.slaplatform.slamanager.api.restbackend.SLAAPI;
import eu.specsproject.slaplatform.slamanager.entities.Collection;
import eu.specsproject.slaplatform.slamanager.entities.Item;
import eu.specsproject.slaplatform.slamanager.entities.SLADocument;
import eu.specsproject.slaplatform.slamanager.entities.SLAIdentifier;

@Path("/slas")
public class SLAsResource {

    @Context
    UriInfo uriInfo; 


    @GET
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public Response getSLAS(@QueryParam("items") Integer totalItems, @QueryParam("page") Integer page, @QueryParam("length") Integer length, @QueryParam("state") String state, @QueryParam("customer") String customer) {

        if(page == null && length == null && totalItems == null){
            totalItems = 50;
        }
        
        List<Item> items = new ArrayList<Item>();
        int start = (page != null && length != null && totalItems == null) ? page : 0; 
        int stop = (totalItems != null) ? totalItems : -1; 
        stop = (page != null && length != null && totalItems == null) ? page+length : stop; 
        
        List <SLAIdentifier> slas = SLAAPI.getInstance().getManager().search(state, customer, start, stop);
        

        stop = slas.size();
        String absolutePath = uriInfo.getAbsolutePath().toString();
        for (int j = 0; j < stop-start; j++){
            String slaId  = slas.get(j).id;
            items.add(new Item(slaId, absolutePath+"/"+slaId));
        }
        Collection collection = new Collection("SLA", slas.size(), stop-start, items);
        
        GenericEntity<Collection> coll = new GenericEntity<Collection>(collection) {
            //Anonimous class
        };
        return Response.ok(coll).build();

    }

    @POST
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public Response createSLA(String document){

        SLAIdentifier id = SLAAPI.getInstance().getManager().create(new SLADocument(document)); 
        UriBuilder ub = uriInfo.getAbsolutePathBuilder();
        URI slaURI = ub.
                path(id.id).
                build();
        return Response.created(slaURI).entity(id.id).build();
    }

    @Path("/{id}")
    public SLAResource getSLAresource(@PathParam("id") String id) {
        return new SLAResource(id);
    }

}