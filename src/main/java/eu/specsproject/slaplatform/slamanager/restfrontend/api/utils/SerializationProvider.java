/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.slaplatform.slamanager.restfrontend.api.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import eu.specsproject.slaplatform.slamanager.internal.marshalling.EntityBuilder;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.EntityMarshaller;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.MarshallingInterface;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.JSONentityBuilder;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.JSONmarshaller;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.XMLentityBuilder;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.implementation.XMLmarshaller;

@Provider
@Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
public class SerializationProvider implements MessageBodyWriter <MarshallingInterface> , MessageBodyReader<MarshallingInterface>{

    @Override
    public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2,
            MediaType arg3) {
        return MarshallingInterface.class.isAssignableFrom(arg0);
    }

    @Override
    public void writeTo(MarshallingInterface arg0, Class<?> arg1, Type arg2,
            Annotation[] arg3, MediaType arg4,
            MultivaluedMap<String, Object> arg5, OutputStream arg6)
            throws IOException {
            
            EntityMarshaller marshaller=null;
        
            BufferedWriter bw = new BufferedWriter( new OutputStreamWriter(arg6));
                        
            if(arg4.equals(MediaType.APPLICATION_JSON)){
                marshaller = new JSONmarshaller();
            }else{
                marshaller = new XMLmarshaller();
            }
            
            @SuppressWarnings("unchecked")//should be OK
            String entity = marshaller.marshal(arg0,(Class<MarshallingInterface>)arg1);
            
            bw.write(entity);
            bw.flush();
            bw.close();
    }

    @Override
    public boolean isReadable(Class<?> arg0, Type arg1, Annotation[] arg2,
            MediaType arg3) {
        return MarshallingInterface.class.isAssignableFrom(arg0);
    }

    @Override
    public MarshallingInterface readFrom(
            Class<MarshallingInterface> arg0, Type arg1, Annotation[] arg2,
            MediaType arg3, MultivaluedMap<String, String> arg4,
            InputStream arg5) throws IOException {

        EntityBuilder builder = null;


        if(arg3.equals(MediaType.APPLICATION_JSON)){
            builder = new JSONentityBuilder();
        }else{
            builder = new XMLentityBuilder();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(arg5));
        StringBuilder entity = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            entity.append(line);
        }

        reader.close();
        
        return builder.unmarshal(entity.toString(), arg0);
    }

    //this is a JAR-RS deprecated method
    //it's here for compatibility issues
    @Override
    public long getSize(MarshallingInterface arg0, Class<?> arg1, Type arg2,
            Annotation[] arg3, MediaType arg4) {
        return 0;
    }
}
