package eu.specsproject.slaplatform.slamanager.restfrontend.api.utils;


public class TestPersistence {

    private static boolean enableTest = false;

    public static boolean isEnableTest() {
        return enableTest;
    }

    public static void setEnableTest(boolean enableTest) {
        TestPersistence.enableTest = enableTest;
    }
}
