package eu.specsproject.slaplatform.slamanager.test;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.core.MediaType;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.test.framework.AppDescriptor;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;

import eu.specsproject.slaplatform.slamanager.SLAManager;
import eu.specsproject.slaplatform.slamanager.SLAManagerFactory;
import eu.specsproject.slaplatform.slamanager.api.restbackend.SLAAPI;
import eu.specsproject.slaplatform.slamanager.api.restfrontend.SLAAnnotationsResource;
import eu.specsproject.slaplatform.slamanager.api.restfrontend.SLAResource;
import eu.specsproject.slaplatform.slamanager.entities.SLADocument;
import eu.specsproject.slaplatform.slamanager.internal.EUSLAMangerSQLJPA;
import eu.specsproject.slaplatform.slamanager.internal.marshalling.MarshallingInterface;
import eu.specsproject.slaplatform.slamanager.restfrontend.api.utils.SerializationProvider;
import eu.specsproject.slaplatform.slamanager.restfrontend.api.utils.TestPersistence;
import eu.specsproject.slaplatform.slamanager.entities.*;


public class SLAManagerApiTest extends JerseyTest { 
	
	private static SLAIdentifier id;
	private static SLAAPI slaapi;
	private static SLAResource slaResource;
	private static SLAManager manager;
	private static SLAAnnotationsResource annot;
	private static SerializationProvider sp;
	
	private static int counter = 0;


    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");
        TestPersistence.setEnableTest(true);
        JerseyEmbeddedHTTPServerCrunchify.startServer();
        manager= (EUSLAMangerSQLJPA) SLAAPI.getInstance().getManager();
        counter++;
        id = manager.create(new SLADocument(readFile("src/test/resources/sla_example.xml")));
        slaResource = new SLAResource(id.id);
        sp = new SerializationProvider();
        Assert.assertNotNull(id);
    }
    
    private void createSlaResource(){
        id = manager.create(new SLADocument());
        slaResource = new SLAResource(id.id);
        counter++;
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //do nothing
    }


    @Before
    public void setUp() throws Exception {
        //System.out.println("setUp Called");
        Assert.assertNotNull(manager);
        Assert.assertNotNull(slaResource);
    }

    @After
    public void tearDown() throws Exception {
        //System.out.println("tearDown Called");
    }
  
    @Test
    public void testSLAAPI() {
        slaapi = SLAAPI.getInstance();
        Assert.assertEquals(SLAAPI.getInstance(), slaapi);
        slaapi = SLAAPI.getInstance();
        Assert.assertEquals(SLAAPI.getInstance(),slaapi);
        Assert.assertNotNull(slaapi.getManager());
    }
    
    
    @Test
    public void testAnnotation()  {
        annot = new SLAAnnotationsResource(id);
        Assert.assertNotNull(annot.getAnnotations());
    }
    
    
    public void testCreateAnnotation()  {
        annot = new SLAAnnotationsResource(id);
        annot.createAnnotation("testId", "testDescription", "testTimestamp");
    }
    
    @Test
    public final void testAlerted() {
        createSlaResource();
        Assert.assertEquals(slaResource.getSLAstatus(), "PENDING");
        slaResource.getAndLockSLA();
        Assert.assertEquals(slaResource.getSLAstatus(), "PENDING");
        slaResource.signSLA("newSLASIGNED");
        Assert.assertEquals(slaResource.getSLAstatus(), "SIGNED");
        slaResource.observeSLA();
        Assert.assertEquals(slaResource.getSLAstatus(), "OBSERVED");
        slaResource.signalAlert();
        Assert.assertEquals(slaResource.getSLAstatus(), "ALERTED");
    }
    
    @Test
    public void testTerminate(){
        createSlaResource();
        Assert.assertEquals(slaResource.getSLAstatus(), "PENDING");
        slaResource.signSLA("newSLASIGNED");
        Assert.assertEquals(slaResource.getSLAstatus(), "SIGNED");
        slaResource.observeSLA();
        Assert.assertEquals(slaResource.getSLAstatus(), "OBSERVED");
        slaResource.terminateSLA();
        Assert.assertEquals(slaResource.getSLAstatus(), "TERMINATED");
    }
    
    @Test
    public final void testRenegotiate(){
        createSlaResource();
        Assert.assertEquals(slaResource.getSLAstatus(), "PENDING");
        slaResource.signSLA("newSLASIGNED");
        Assert.assertEquals(slaResource.getSLAstatus(), "SIGNED");
        slaResource.observeSLA();
        Assert.assertEquals(slaResource.getSLAstatus(), "OBSERVED");
        slaResource.reNegotiate();
        Assert.assertEquals(slaResource.getSLAstatus(), "RENEGOTIATING");
    }
    
    @Test
    public final void testComplete(){
        createSlaResource();
        Assert.assertEquals(slaResource.getSLAstatus(), "PENDING");
        slaResource.signSLA("newSLASIGNED");
        Assert.assertEquals(slaResource.getSLAstatus(), "SIGNED");
        slaResource.observeSLA();
        Assert.assertEquals(slaResource.getSLAstatus(), "OBSERVED");
        slaResource.completeSLA();
        Assert.assertEquals(slaResource.getSLAstatus(), "COMPLETED");
    }
   
    
    @Test
    public final void testAnnotationResource() {
        createSlaResource();
        Assert.assertNotNull(slaResource.getAnnotationResource());
    }
    
    @Test
    public final void testUpdateSLA() {
        createSlaResource();
        Assert.assertEquals(slaResource.updateSLA("xml"),id.id);
        Assert.assertEquals(slaResource.updateSLA(null),String.valueOf(id));
        Assert.assertEquals(slaResource.getSLA(),"xml");
    }

      // rivedere
    @Test
    public final void testUnlockSLA() {
        slaResource = new SLAResource(id.id);
        slaResource.unlockSLA();
    } 


    
    @Test
    public void testIsWritable() throws Exception{
        Assert.assertTrue(sp.isWriteable(MarshallingInterface.class, null,null,null));
    }
    
    @Test
    public void testIsReadeble() throws Exception{
        Assert.assertTrue(sp.isReadable(MarshallingInterface.class, null,null,null));
    }
    
    @Test
    public void testGetSizee() throws Exception{
        Assert.assertEquals(sp.getSize(new MarshallingInterface(){}, MarshallingInterface.class, null, null, null),0);
    }  
    
    @Test
    public void testReadFromXMLEntityBuilder() throws IOException{
        String xml = "<string name=string_name>text_string</string>";
        InputStream is = new ByteArrayInputStream(xml.getBytes());
        Assert.assertNull(sp.readFrom(MarshallingInterface.class, null, null, MediaType.APPLICATION_XML_TYPE,null, is));
    }
    
     @Override
     protected AppDescriptor configure() {
        return new WebAppDescriptor.Builder().build();
    }
    
     @Test
     public void testSLAsResource(){
        createSlaResource();
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        
        WebResource web = client.resource("http://localhost:8089/slas");
        ClientResponse response = web.type(MediaType.TEXT_XML).post(ClientResponse.class,"documento");
        String responsePost = response.getEntity(String.class);
        Assert.assertNotNull(responsePost);
        Assert.assertEquals(201,response.getStatus());
        web = client.resource("http://localhost:8089/slas/"+responsePost);
        response = web.get(ClientResponse.class);
        String responseGet = response.getEntity(String.class);
        Assert.assertEquals(200,response.getStatus());
        Assert.assertEquals("documento",responseGet);
     }  
     
     
     
     @Test
     public void testSLAsResourcesGetSLAS(){
        createSlaResource();
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource web = client.resource("http://localhost:8089/slas?items=1&page=2&length=4");
        ClientResponse response = web.get(ClientResponse.class);
        String responseGet = response.getEntity(String.class);
        Assert.assertEquals(200,response.getStatus());
        Assert.assertNotNull(responseGet);
     }
     
     public static String readFile(String fileName) throws IOException {
         BufferedReader br = new BufferedReader(new FileReader(fileName));
         try {
             StringBuilder sb = new StringBuilder();
             String line = br.readLine();

             while (line != null) {
                 sb.append(line);
                 sb.append("\n");
                 line = br.readLine();
             }
             return sb.toString();
         } finally {
             br.close();
         }
     }
}
